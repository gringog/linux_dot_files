
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions



#
# prompt
#
red="\[\e[0;31m\]"          # red
green="\[\e[0;32m\]"        # green
yellow="\[\e[0;33m\]"       # yellow
blue="\[\e[0;34m\]"         # blue
magenta="\[\e[0;35m\]"      # magenta
cyan="\[\e[0;36m\]"         # cyan
white="\[\e[0;37m\]"        # white

bred="\[\e[1;31m\]"         # bold red
bgreen="\[\e[1;32m\]"       # bold green
byellow="\[\e[1;33m\]"      # bold yellow
bblue="\[\e[1;34m\]"        # bold blue
bmagenta="\[\e[1;35m\]"     # bold magenta
bcyan="\[\e[1;36m\]"        # bold cyan
bwhite="\[\e[1;37m\]"       # bold white

if [ `id -u` -eq "0" ]; then
  root="${yellow}"
else
  root="${red}"
fi

PS1="${red}[${root}\u${green}@\[\e[0;96m\]\h${red}][${green}\w${red}]\n${red}└─╼ \[\e[0m\]"


#
# path
#
[[ -d ~/bin ]] && export PATH=~/bin:$PATH   # local binaries & scripts


#
# hystory
#
export HISTSIZE=5000                            # Increase bash history size
export HISTCONTROL=ignoreboth                   # ingore space and duplicates in history
export PROMPT_COMMAND='history -a'              # save command after it has been executed
export HISTIGNORE="&:[ ]*:ls:ls -a:cd:cd .."    # leave commands out of history log



#
# alias and function from external
#
[ -f ~/.bash_aliases ] && source ~/.bash_aliases          # aliases
[ -f ~/.bash_functions ] && source ~/.bash_functions      # functions
