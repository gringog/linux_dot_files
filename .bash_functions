#-------------------------------------------------------------
# File & strings related functions:
#-------------------------------------------------------------

# Find a file with a pattern in name:
function ff() { find . -type f -iname '*'"$*"'*' -ls ; }

function extract()      # Handy Extract Program
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Create a ZIP archive of a file or folder.
function makezip() { zip -r "${1%%/}.zip" "$1" ; }

function _exit()              # Function to run upon exit of shell.
{
    echo -e "${BRed}Hasta la vista, baby${NC}"
}
trap _exit EXIT

#-------------------------------------------------------------
# Process/system related functions:
#-------------------------------------------------------------


function my_ip() # Get IP adress on ethernet.
{
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' |
      sed -e s/addr://)
    echo ${MY_IP:-"Not connected"}
}

function ii()   # Get current host related info.
{
    echo -e "\nYou are logged on ${BRed}$HOST"
    echo -e "\n${BRed}Additionnal information:$NC " ; uname -a
    echo -e "\n${BRed}Machine stats :$NC " ; uptime
    echo -e "\n${BRed}Memory stats :$NC " ; free
    echo -e "\n${BRed}Diskspace :$NC " ; mydf / $HOME
    echo -e "\n${BRed}Local IP Address :$NC" ; my_ip
    echo -e "\n${BRed}Open connections :$NC "; netstat -pan --inet;
    echo
}


#
# navigation and basic operations
#
# mkcd - makedir and cd in it
# usage: mkcd <file>
mkcd () 
{ 
    mkdir $1 && cd $1 
}

# cpcd - cp and cd to destination
# usage: cpcd <cp-arguments>
cpcd ()
{
    cp $@ && cd ${!#}
}

# mvcd - mv and cd to destination
# usage: mvcd <mv-arguments>
mvcd ()
{
    mv $@ && cd ${!#}
}

# cdls - cd and ls
# usage: cdls <path>
cdls()
{
    cd $@;
    ls -a --color=auto
}

# up - cd up by n directories
# usage: up <level-numbers>
up()
{
    dir=""
    if [ -z "$1" ]; then
        dir=..
    elif [[ $1 =~ ^[0-9]+$ ]]; then
        x=0
        while [ $x -lt ${1:-1} ]; do
            dir=${dir}../
            x=$(($x+1))
        done
    else
        dir=${PWD%/$1/*}/$1
    fi
    cd "$dir";
}


# lowercase - rename file names to lowercase and replace space with underscore from pwd
# usage: lowercase
lowercase()
{
    # Process each directory’s contents before the directory  itself
    find * -depth -type d | while read x
    do
        # Translate Caps to Small letters
        y=$(echo "$x" | tr '[A-Z ]' '[a-z_]');
        # create directory if it does not exit
        if [ ! -d "$y" ]; then
                mkdir -p "$y"
        fi
        # check if the source and destination is the same
        if [ "$x" != "$y" ]; then
                # move directory files before deleting
                ls -A "$x" | while read i
                do
                  mv "$x"/"$i" "$y"
                done
                rmdir "$x"
        fi
    done
    # Rename all files
    find * -type f | while read x
    do
        # Translate Caps to Small letters
        y=$(echo "$x" | tr '[A-Z ]' '[a-z_]')
        if [ "$x" != "$y" ]; then
                mv "$x" "$y"
        fi
    done
}


#
# find and grep
#
# fp - find a file by name in pwd
# usage: fp <name>
fp()
{
    sudo find . -iname '*'$*'*' -ls
}

# fr - find a file by name globally
# usage: fr <name>
fr()
{
    sudo find / -iname '*'$*'*' -ls
}

# fcd - find a file by name in selected path
# usage: fcd <name> <path>
fcd()
{
    sudo find $2 -iname '*'$1'*' -ls
}

# fpg - find a file by grepping in pwd
# usage: fpg <word>
fpg()
{
    sudo grep --color=auto -HIrFo -- $* .
}

# frg - find a file by grepping in root
# usage: frg <word>
frg()
{
    sudo grep --color=auto -HIrFo -- $* /
}

# fcdg - find a file by grepping in selected path
# usage: fcdg <word> <path>
fcdg()
{
    sudo grep --color=auto -HIrFo -- $1 $2
}

# mang - search in man page
# usage: mang <manpage> <word>
mang()
{
    man $1 | grep --color=auto $2 -C 5
}

# hig - search in history
# usage: hig <word>
hig()
{
    history | grep --color=auto $* -C 5
}

# psg - check if a process is running by name and return PID(s)
# usage: psg <process-name>
psg()
{
    if ps ax | grep -v grep | grep $1 > /dev/null
    then
        ps ax | grep --color=auto $1
    else
        echo "$1 is not running"
    fi
}



# publicip - get the current public ip address
# usage: publicip
myip()
{ 
    curl ifconfig.me/ip
}


# log - view live and color logs
# usage: log <logname>
log()
{
    sudo tail -f -n 50 $1 | ccze
}


#
# automations
#
# dotfiles - sync dotfiles for git sync
# usage: dotfiles
dots()
{
    rsync -a -v --existing ~ ~/source/linux_dot_files/
}


# pw - generate a random password
# usage: pw <password-length> - defaults to 12
pw() 
{
    echo $(cat /dev/urandom | tr -cd '[:graph:]' | head -c ${1:-12})
}